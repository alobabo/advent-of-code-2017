from collections import namedtuple 
from enum import Enum
import re
import sys

test = "dabAcCaCBAcCcaDA"

f = open("input", "r")
inputs = f.read()
f.close()



# inputs = test
t = list(inputs.strip())

def isReduc(c1, c2):
    return c1.isalpha() and c2.isalpha() and abs(ord(c1) - ord(c2)) == 32


def reduc(t):
    i = 0
    length = len(t)
    while (i < length-1):
        if (i >= 0) and (isReduc(t[i], t[i+1])):
        #Case reduction
            # print(t[i], t[i+1])
            t.pop(i+1)
            t.pop(i)
            i = i - 1
            length = len(t)
        else:
        #Case no reduction
            i += 1
    return t


alphabet = "abcdefghijklmnopqrstuvwxyz"
mini = len(t)

for l in alphabet:
    temp = list(filter(lambda a: a.lower() != l, t))
    temp = reduc(temp)
    sz = len(temp)
    if sz < mini:
        mini = sz
    
print(mini)
