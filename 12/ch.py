from functools import reduce

fname = "test"
fname = "input"

f = open(fname, "r")
inputs = f.read().splitlines()
f.close()

plants = []
combos = {} 
min_plant = -2

for i in range(32):
    combos[i] = "."

def bin_value(values):
    s = 0
    for i,v in enumerate(values):
        if v == "#":
            s += (2 ** i)
    return s

def parse(text):
    global min_plant 
    global combos
    #Initial state
    plants.append(".")
    plants.append(".")
    for plant in text[0][15:]:
        plants.append(plant)
    plants.append(".")
    plants.append(".")
    #Combos
    for c in text[2:]:
        combos[bin_value(c[:5])] = c[9]

def get_combo(iplant):
    combo = ["." for _ in range(5)]
    combo[2] = plants[iplant]
    if iplant > 0:
        combo[1] = plants[iplant-1]
        if iplant > 1:
            combo[0] = plants[iplant-2]
    if iplant < len(plants) - 1:
        combo[3] = plants[iplant+1]
        if iplant < len(plants) - 2:
            combo[4] = plants[iplant+2]
    # print("{} = {}".format("".join(combo), bin_value(combo)))
    return bin_value(combo)

def extreme_plants(pattern, new_plants):
    if pattern == "#." or pattern == "##":
        new_plants.append(".")
        new_plants.append(".")
    if pattern == ".#":
        new_plants.append(".")

def step():
    global min_plant
    global plants
    new_plants = []
    extreme_plants(combos[get_combo(0)] + combos[get_combo(1)], new_plants)  
    min_plant -= len(new_plants)
    for iplant in range(len(plants)):
        new_plants.append(combos[get_combo(iplant)])
    extreme_plants(combos[get_combo(len(plants)-2)] 
            + combos[get_combo(len(plants)-1)], new_plants)  
    plants = new_plants

def count_plants():
    c = 0
    for i in range(len(plants)):
        if plants[i] == "#":
            c += i + min_plant
    return c

parse(inputs)
for i in range(120):
    step()
    new_count = count_plants()
    if i > 100:
        print("{}: {}".format(i+1, new_count - old_count))
    old_count = new_count
    # print("{}: {}".format(i+1, "".join(plants)))

# print(count_plants())
print((50000000000-120) * 21 + 3000) 

