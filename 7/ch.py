from collections import namedtuple
from functools import reduce

test = [
"Step C must be finished before step A can begin.",
"Step C must be finished before step F can begin.",
"Step A must be finished before step B can begin.",
"Step A must be finished before step D can begin.",
"Step B must be finished before step E can begin.",
"Step D must be finished before step E can begin.",
"Step F must be finished before step E can begin.",
 ]

f = open("input", "r")
inputs = f.read().splitlines()
f.close()


class Node:
    def __init__(self, time, prevs, nexts):
        self.time = time
        self.prevs = prevs
        self.nexts = nexts

nodes = {}

nb_worker = 5
less = 4

# nb_worker = 2
# less = 64
# inputs = test

def parse(line):
    global nodes
    line = line.split()
    Id = line[1]
    nex = line[7]
    if Id not in nodes.keys():
        nodes[Id] = Node(ord(Id)-less, [], [nex])
    else:
        nodes[Id].nexts.append(nex)
    if nex not in nodes.keys():
        nodes[nex] = Node(ord(nex)-less, [Id], [])
    else:
        nodes[nex].prevs.append(Id)

def find_root():
    for (name, node) in nodes.items():
        if len(node.prevs) == 0:
            return name

def pick_next():
    global available
    # next is the first in available
    cname = None
    if len(available) != 0:
        cname = available[0]
        # remove from available
        available.pop(0)
    return cname

def can_be_available(name):
    return len(nodes[name].prevs) == 0

class Worker:
    def __init__(self):
        self.node = None
        self.has_processed = False

    def process(self):
        if self.node is not None:
            if nodes[self.node].time > 0:  #Still some time left
                self.dec_time()
                self.has_processed = True
            else:  #Finished processing
                self.end_processing()

    def pick_new_node(self):
        nxt = pick_next()
        if nxt is not None: #a node is available
            self.node = nxt
            self.dec_time()
        else:
            self.node = nxt

    def end_processing(self):
        global available
        for nname in nodes[self.node].nexts:
            # remove from the next node the chosen node
            nodes[nname].prevs.remove(self.node)
            # if in the next nodes there are no prevs then add it to 
            # the available nodes
            if can_be_available(nname):
                available.append(nname)
                available.sort()

    def dec_time(self):
        nodes[self.node].time -= 1


def is_finished():
    # print(list(map(lambda x: x.node, workers)))
    # print(reduce(lambda a,b: a and (b.node is None), workers, True))
    return reduce(lambda a,b: a and (b.node is None), workers, True)

def print_state():
    print("{}\t".format(time), end="")
    for w in workers:
        print(w.node, end=" ")
    # print(available, end="")
    print()

for l in inputs:
    parse(l)
for node in nodes.values():
    node.prevs.sort()
    node.nexts.sort()

root = find_root()
available =  sorted(list(filter(can_be_available, nodes.keys())))
time = 0

workers = [ Worker() for _ in range(nb_worker) ]
    
for worker in workers:
    worker.pick_new_node()
while (not is_finished()):
# for _ in range(16):
    print_state()
    for worker in workers:
        worker.process()
    for worker in workers:
        if not worker.has_processed:
            worker.pick_new_node()
    for worker in workers:
        worker.has_processed = False
    time += 1

print(time)

