

test = [3, -2, 5, -9]
f = open("input1", "r")
inputs = f.read().splitlines()
inputs = [ int(i) for i in inputs ]
f.close()
traces = [0]
s = 0
i = 0

while True:
    s += inputs[i]
    if s in traces:
        print(s)
        exit() 
    traces.append(s)
    i += 1
    if i == len(inputs):
        i = 0
        # print(traces)


