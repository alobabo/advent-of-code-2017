from enum import Enum
from copy import copy
import sys
sys.path.insert(1, "..")

fname = "input"
# fname = "test"

f = open(fname,"r")
puzzle = f.read().splitlines()
f.close()
s = [5531103, 0, 0, 0, 0, 0]

class Instr():
    def __init__(self, name, a, b, c):
        self.name = name
        self.a = a
        self.b = b
        self.c = c
        self.run = instrs[name]

    def ex(self):
        s[self.c] = self.run(self.a,self.b)

    def __repr__(self):
        return "{} {} {} {}".format(self.name, self.a, self.b, self.c)

instrs = {
        "addr": (lambda a,b:  s[a] + s[b]),
        "addi"  : lambda a,b:  s[a] + b, 
        "mulr"  : lambda a,b:  s[a] * s[b], 
        "muli"  : lambda a,b:  s[a] * b, 
        "banr"  : lambda a,b:  s[a] & s[b], 
        "bani"  : lambda a,b:  s[a] & b, 
        "borr"  : lambda a,b:  s[a] | s[b], 
        "bori"  : lambda a,b:  s[a] | b, 
        "setr"  : lambda a,b:  s[a], 
        "seti"  : lambda a,b:  a, 
        "gtir"  : lambda a,b:  1 if a > s[b] else 0 , 
        "gtri"  : lambda a,b:  1 if s[a] > b else 0, 
        "gtrr"  : lambda a,b:  1 if s[a] > s[b] else 0 , 
        "eqir"  : lambda a,b:  1 if a == s[b] else 0 , 
        "eqri"  : lambda a,b:  1 if s[a] == b else 0 , 
        "eqrr"  : lambda a,b:  1 if s[a] == s[b] else 0 , 
        }


#Set instruction pointer
ip = int(puzzle[0][4])

#transform the lines into list of instructions
code = puzzle[1:]
for i, line in enumerate(code):
    name, a, b, c = line.split()
    code[i] = Instr(name, int(a), int(b), int(c))

def translate_instr(i):
    res = None
    if i.name == "addr":
        res = "x{} = x{} + x{}".format(i.c, i.a, i.b)
    elif i.name == "addi":
        res = "x{} = x{} + {}".format(i.c, i.a, i.b)
    elif i.name == "mulr":
        res = "x{} = x{} * x{}".format(i.c, i.a, i.b)
    elif i.name == "muli":
        res = "x{} = x{} * {}".format(i.c, i.a, i.b)
    elif i.name == "banr":
        res = "x{} = x{} & x{}".format(i.c, i.a, i.b)
    elif i.name == "bani":
        res = "x{} = x{} & {}".format(i.c, i.a, i.b)
    elif i.name == "borr":
        res = "x{} = x{} | x{}".format(i.c, i.a, i.b)
    elif i.name == "bori":
        res = "x{} = x{} | {}".format(i.c, i.a, i.b)
    elif i.name == "setr":
        res = "x{} = x{}".format(i.c, i.a)
    elif i.name == "seti":
        res = "x{} = {}".format(i.c, i.a)
    elif i.name == "gtir":
        res = "x{} = 1 if {} > x{} else 0".format(i.c, i.a, i.b)
    elif i.name == "gtri":
        res = "x{} = 1 if x{} > {} else 0".format(i.c, i.a, i.b)
    elif i.name == "gtrr":
        res = "x{} = 1 if x{} > x{} else 0".format(i.c, i.a, i.b)
    elif i.name == "eqir":
        res = "x{} = 1 if {} == x{} else 0".format(i.c, i.a, i.b)
    elif i.name == "eqri":
        res = "x{} = 1 if x{} == {} else 0".format(i.c, i.a, i.b)
    elif i.name == "eqrr":
        res = "x{} = 1 if x{} == x{} else 0".format(i.c, i.a, i.b)
    else:
        print("Instructions not found")
    return res
	
def translate_code(code):
    new_code = []
    for line, i in enumerate(code):
        new_instr = translate_instr(i)
        if i.c == ip:
             new_instr = new_instr.replace("x{} = ".format(str(ip)), "goto ")
        new_instr = new_instr.replace("x{}".format(str(ip)), str(line))
        new_instr = "{}:\t{}".format(line, new_instr)
        new_code.append(new_instr)
    return new_code


# new_code = translate_code(code)
# print("\n".join(new_code))

c = 0
while (s[ip] >= 0 and s[ip] < len(code)):
    step = 1
    instr = code[s[ip]]
    if (c%step == 0):
        print("ip={} {} {}".format(ip, s, instr), end =" ")
    instr.ex()
    if (c%step == 0):
        print(s)
    s[ip] = s[ip] + 1
    c = c + 1

# print(s)
# print("Number of steps:" ,c)

