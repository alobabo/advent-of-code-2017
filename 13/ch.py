import sys
sys.path.insert(1, "..")
from grid import *
from functools import reduce
from enum import Enum
import copy

fname = "test"
fname = "input"

f = open(fname, "r")
inputs = f.read().splitlines()
f.close()


class Dir(Enum):
    LEFT=0
    TOP=1
    RIGHT=2
    BOT=3

class CDir(Enum):
    LEFT=0
    STRAIGHT=1
    RIGHT=2

def parse(lines):
    width = len(lines[0])
    height = len(lines)
    grid = Grid(width, height, None)
    carts = []
    for y,line in enumerate(lines):
        for x,c in enumerate(line):
            if c == ">":
                grid.set_elmt(x,y, "-")
                carts.append(Cart(x,y, Dir.RIGHT))
            elif c == "<":
                grid.set_elmt(x,y, "-")
                carts.append(Cart( x,y, Dir.LEFT))
            elif c == "v":
                grid.set_elmt(x,y, "|")
                carts.append(Cart( x,y, Dir.BOT))
            elif c == "^":
                grid.set_elmt(x,y, "|")
                carts.append(Cart(x,y, Dir.TOP))
            else:
                grid.set_elmt(x,y, c)
    return (grid , carts)

def print_map():
    new_grid = copy.deepcopy(grid)
    for c in carts:
        if c.cdir == Dir.BOT:
            vc = "v"
        if c.cdir == Dir.TOP:
            vc = "^"
        if c.cdir == Dir.LEFT:
            vc = "<"
        if c.cdir == Dir.RIGHT:
            vc = ">"
        new_grid.set_elmt(c.x, c.y, vc)
    new_grid.print_grid()

class Cart:
    def __init__(self, x, y, cdir):
        self.x = x
        self.y = y
        self.cdir = cdir
        self.nturn = CDir.LEFT
        self.has_moved = False

    def __str__(self):
        return "Cart(({},{}), cdir={}, ndir={})".format(
                self.x, self.y, self.cdir, self.nturn)

    def step(self, grid):
        #Next position
        if self.cdir == Dir.BOT:
            self.y += 1
        elif self.cdir == Dir.TOP:
            self.y -= 1
        elif self.cdir == Dir.LEFT:
            self.x -= 1
        elif self.cdir == Dir.RIGHT:
            self.x += 1
        #Next direction
        path = grid.get_elmt(self.x, self.y)
        if path == "+":
            self.cdir = Dir((self.cdir.value + self.nturn.value - 1)%4)
            self.nturn = CDir((self.nturn.value + 1) % 3)
        elif path == "\\":
            if self.cdir == Dir.BOT:
                self.cdir = Dir.RIGHT
            elif self.cdir == Dir.TOP:
                self.cdir = Dir.LEFT
            elif self.cdir == Dir.LEFT:
                self.cdir = Dir.TOP
            elif self.cdir == Dir.RIGHT:
                self.cdir = Dir.BOT
        elif path == "/":
            if self.cdir == Dir.BOT:
                self.cdir = Dir.LEFT
            elif self.cdir == Dir.TOP:
                self.cdir = Dir.RIGHT
            elif self.cdir == Dir.LEFT:
                self.cdir = Dir.BOT
            elif self.cdir == Dir.RIGHT:
                self.cdir = Dir.TOP

def has_collision():
    # print(carts, len(carts))
    # print(positions, len(positions))
    global carts
    rm_carts = []
    tcarts = sorted(carts, key= lambda c: (c.x, c.y))
    for i in range(len(carts)-1):
        if tcarts[i].x == tcarts[i+1].x and tcarts[i].y == tcarts[i+1].y:
            rm_carts.append(tcarts[i])
            rm_carts.append(tcarts[i+1])
    return rm_carts



def tick():
    global carts
    carts.sort(key= lambda c: (c.x, c.y))
    rm_carts = []
    for c in carts:
        c.step(grid)
        rm_carts = rm_carts + has_collision()
    carts = list(filter(lambda c: c not in rm_carts, carts))

def print_carts():
    for c in carts:
        print(c)


grid, carts = parse(inputs)
carts.sort(key= lambda c: (c.x, c.y))
while(True):
    # print_map()
    tick()
    if (len(carts) == 1):
        print("{},{}".format(carts[0].x, carts[0].y))
        # print_map()
        break

# for _ in range(20):
    # tick()
    # print_map()
    # print_carts()

