# EAch array element of the main array is a line
import sys
class Grid:
    def __init__(self, width, height, val=".", min_x=0, min_y=0):
        self.width = width
        self.height = height
        self.min_x = min_x
        self.min_y = min_y
        self.grid = [[ val for _ in range(0,min_x+width) ] for _ in range(0,min_y+height)]

    def print_grid(self, sep="", show=lambda x: x):
        for y in range(self.min_y, self.min_y+self.height):
            for x in range(self.min_x, self.min_x+self.width):
                element = self.get_elmt(x,y)
                print(show(element), end=sep)
            print()
        print()

    def set_elmt(self, x, y, val):
        if x >= self.min_x and y >= self.min_y and \
                x < self.width+self.min_x and y < self.height+self.min_y:
            self.grid[y][x] = val
        else:
            print("({}, {}) out of bounds x = [{},{}[, y = [{},{}[".format(x,y,\
                    self.min_x, self.width+self.min_x, self.min_y, self.height+self.min_y))
            self.print_grid()
            sys.exit()

    def get_elmt(self, x, y):
        # try:
            # return self.grid[y][x]
        # except:
            # return None
        if x >= self.min_x and y >= self.min_y and \
                x < self.width+self.min_x and y < self.height+self.min_y:
            return self.grid[y][x]
        else:
            return None
        

