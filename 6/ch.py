from collections import namedtuple 
from enum import Enum
import sys
sys.path.append('..')
from grid import *

test = [
"1, 1",
"1, 6",
"8, 3",
"3, 4",
"5, 5",
"8, 9",
 ]

f = open("input", "r")
inputs = f.read().splitlines()
f.close()

# inputs = test

Root = namedtuple("Root", ["id", "x", "y"])

xs = [ int(cell.split(",")[0]) for cell in inputs ]
ys = [ int(cell.split(",")[1]) for cell in inputs ]
roots = [ Root(i, xs[i], ys[i]) for i in range(len(inputs)) ]

width = max(xs) + 1
heigth = max(ys) + 1
grid = Grid(width, heigth, val=None)


def init_cell(x, y, root):
    global grid
    distance = abs(y - root.y) + abs(x - root.x)
    cell = grid.get_elmt(x,y)
    if cell is None:
        cell = {root.id: distance}
        grid.set_elmt(x,y, cell)
    else:
        cell[root.id] = distance

def init_grid():
    global grid
    for y in range(grid.heigth):
        for x in range(grid.width):
    # for y in range(2):
        # for x in range(1):
            for root in roots:
                init_cell(x, y, root)

def get_closests(cell):
    mini = min(cell.values())
    closests = []
    for k, v in cell.items():
        if v == mini:
            closests.append(k)
    return closests

def get_closest(cell):
    closests = get_closests(cell)
    if len(closests) > 1:
        return None
    else:
        return closests[0]

def print_cell(cell):
    c = get_closest(cell)
    return str(c) if (c is not None) else  "."

def is_closest_in(root, coordinates):
    for coordinate in coordinates:
        cell = grid.get_elmt(coordinate[0], coordinate[1])
        closest = get_closest(cell)
        if (closest is not None) and (root.id == closest):
            return True
    return False


def is_root_inf(root):
    top = [ (i, 0) for i in range(width) ]
    bot = [ (i, heigth-1) for i in range(width) ]
    l = [ (0, i) for i in range(heigth) ]
    r = [ (width-1, i) for i in range(heigth) ]
    return is_closest_in(root, top) or \
            is_closest_in(root, bot) or\
            is_closest_in(root, l) or\
            is_closest_in(root, r) 

def count_closest(root):
    c = 0
    for x in range(width):
        for y in range(heigth):
            cell  = grid.get_elmt(x,y)
            if get_closest(cell) == root.id:
                c+=1
    return c

init_grid()
# print(roots)
# print(grid.get_elmt(0,0))
# print(grid.get_elmt(1,1))

# grid.print_grid(sep="\t", show=(lambda x: print_cell(x)))

count = 0
for x in range(width):
    for y in range(heigth):
        cell = grid.get_elmt(x,y)
        s = sum(cell.values())
        if s < 10000:
            count += 1
print(count)





