fec = 0
sec = 1
recipes = [3,7]
find = False  
puzzle = list(map(int,str(18)))
length = len(puzzle)
while not find:
    sumDig = recipes[fec] + recipes[sec]
    newRe = list(map(int,str(sumDig)))
    recipes += newRe
    
    fec = (1 + recipes[fec] + fec) % len(recipes) 
    sec = (1 + recipes[sec] + sec) % len(recipes)

    #print(len(recipes))
    if len(recipes) > length and recipes[-length: ] == puzzle:
        print("index: ", len(recipes) - length)
        find = True
