test  = [
        "abcde",  
        "fghij",
        "klmno",
        "pqrst",
        "fguij",
        "axcye",
        "wvxyz",
        ]

f = open("input", "r")
inputs = f.read().splitlines()
f.close()

def solve(t):
    sz = len(t[0])
    sums = [ sum([ ord(c) for c in w]) for w in t]
    pairs = []
    for i in range(len(t)):
        for j in range(i+1, len(t), 1):
            if abs(sums[i] - sums[j]) < 27:
                pairs.append((t[i], t[j]))

    print(pairs)
    for p in pairs:
        c = 0
        for i in range(sz):
            if p[0][i] != p[1][i]:
                c += 1
        if c == 1:
            print(p)
            for i in range(sz):
                if p[0][i] == p[1][i]:
                    print(p[0][i], end="")
            print()
            break

solve(inputs)

