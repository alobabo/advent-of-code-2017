from collections import namedtuple 
from enum import Enum
import sys
sys.path.insert(0, '../grid.py')

test = [
"#1 @ 1,3: 4x4",  
"#2 @ 3,1: 4x4",
"#3 @ 5,5: 2x2",
 ]

f = open("input", "r")
inputs = f.read().splitlines()
f.close()


Claim = namedtuple("Claim", ["id", "left", "top", "width", "height"])

def parse(line):
    elts = line.split()
    ID = int(elts[0][1:])
    margins = elts[2].split(",")
    left = int(margins[0])
    top = int(margins[1][:-1])
    size = elts[3].split("x")
    width = int(size[0])
    height = int(size[1])
    ret = Claim(ID, left, top, width, height)
    return ret


def fill_grid(claim):
    global grid
    fail = False
    for h in range(claim.height):
        for w in range(claim.width):
            if grid[w+claim.left][h+claim.top] == 0:
                grid[w+claim.left][h+claim.top] = claim.id
            else: 
                grid[w+claim.left][h+claim.top] = "X"

    
def check_claim(claim):
    for h in range(claim.height):
        for w in range(claim.width):
            if grid[w+claim.left][h+claim.top] != claim.id:
                return False
    return True

claims = []
count = 0
# grid = [ [0 for _ in range(10)] for _ in range(10)]
# inputs = test
grid = [ [0 for _ in range(1000)] for _ in range(1000)]
for i in inputs:
    claims.append(parse(i))
for c in claims:
    fill_grid(c)

# print_grid(grid)

for claim in claims:
    if check_claim(claim):
        print(claim.id)
        exit()

