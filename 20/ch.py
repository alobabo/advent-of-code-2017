

f = open("input", "r")
val = f.read()
f.close()
# val = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$"
# val = "^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$"
# val = "^abc(de(ta|te)|fg)toto$"
# val = "^abc(de|fg)toto$"
# val = "^abc(de|fg)$"
# val = "^abctoto$"
val = val[1:-1]

class Tree:
    queue = []
    def __init__(self, node):
        self.node = node
        self.children = []

    def add_child(self,child):
        self.children.append(child)

    def fold(self, f, acc):
        for c in self.children:
            acc = c.fold(f, acc)
        acc = f(node, acc)
        return acc

    def get_leaves(self):
        acc = []
        self.get_leaves_rec(acc)
        return acc

    def get_leaves_rec(self, acc):
        if len(self.children) == 0:
            acc.append(self)
        else:
            for c in self.children:
                c.get_leaves_rec(acc)

    #return an array containing all possible paths of a tree
    def get_paths(self):
        # if we have a leaf we return the only existing path
        if len(self.children) == 0:
            return [self.node]
        else:
            paths = []
        for c in self.children:
            paths = paths + c.get_paths()
        return list(map(lambda p: self.node + p, paths))

    def __repr__(self):
        res = self.node + " --- "
        children = list(map(lambda c: c.node, self.children))
        res = res + ", ".join(children) + "\n"
        for c in self.children:
            res += c.__repr__()
        return res

            

#Parse return the leaves of a tree
def parse(ite, parent) :
    trees = [Tree("")]
    current_tree = trees[-1]
    c = True
    while c is not None:
        c = next(ite, None)
        if c == "(":
            current_tree = parse(ite, current_tree)
        elif c == ")":
            parent.children = trees
            leaves = parent.get_leaves()
            next_tree = Tree("")
            next_trees = [next_tree]
            for l in leaves:
                l.children = next_trees
            return next_tree
        elif c == "|":
            #New tree is added
            trees.append(Tree(""))
            current_tree = trees[-1]
        else:
            # string has ended
            if c is None:
                break
            current_tree.node += c
    parent.children = trees
    return current_tree 

ite = iter(val)
tree = Tree("")
parse(ite, tree)

print("PATH = ", val)
# print(tree)
paths = tree.get_paths()
print(paths)
maxi = 0
for p in paths:
    if maxi < len(p):
        maxi = len(p)
print(maxi)

