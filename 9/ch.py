
class Block:
    def __init__(self,val, pblock, nblock):
        self.val = val
        self.prev = pblock
        self.next = nblock

t = (429, 7090100)
t1 = (9, 25)
t2 = (10, 1618)
t3 = (13, 7999)
t4 = (17, 1104)
t5 = (21, 6111)
t6 = (30, 5807)

tests = [t1, t2, t3, t4, t5, t6]

def add_marble(val, cblock):
    nblock = cblock.next
    nnblock = nblock.next
    b = Block(val, nblock, nnblock)
    nblock.next = b
    nnblock.prev = b
    # print(b.prev.val, b.val, b.next.val)
    return b


def special_turn(cblock):
    b = cblock
    for _ in range(7):
        b = b.prev
    b.prev.next = b.next
    b.next.prev = b.prev
    val = b.val
    b = b.next
    # print(b.prev.val, b.val, b.next.val)
    return (val, b)

    
def init_points(max_marble):
    b0 = Block(0, None, None)
    b1 = Block(1, b0, b0)
    b0.prev = b1
    b0.next = b1
    cblock = b1
    points = {}
    for val in range(2,max_marble+1):
        # print(marbles, marbles[pos])
        if val % 23 != 0:
            cblock = add_marble(val, cblock)
        else: 
            tmp, cblock = special_turn(cblock)
            points[val] = tmp + val
    return points


def count_points(points, nb_players):
    score = {k: 0 for k in range(nb_players)}
    for k,v in points.items():
        player = k % nb_players
        score[player] += v
    return score

inputs = tests[0:1]
inputs = tests
inputs = [t]

for test in inputs:
    points = init_points(test[1])
    score = count_points(points, test[0])
    # print(score)
    print("high score is: {} by player {}".format(
        max(score.values()), 
        max(score.keys(), key=lambda k: score[k])))


        

