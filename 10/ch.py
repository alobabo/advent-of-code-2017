import sys
from functools import reduce
sys.path.insert(1, "..")
from grid import *

test = [
"position=< 9,  1> velocity=< 0,  2>",
"position=< 7,  0> velocity=<-1,  0>",
"position=< 3, -2> velocity=<-1,  1>",
"position=< 6, 10> velocity=<-2, -1>",
"position=< 2, -4> velocity=< 2,  2>",
"position=<-6, 10> velocity=< 2, -2>",
"position=< 1,  8> velocity=< 1, -1>",
"position=< 1,  7> velocity=< 1,  0>",
"position=<-3, 11> velocity=< 1, -2>",
"position=< 7,  6> velocity=<-1, -1>",
"position=<-2,  3> velocity=< 1,  0>",
"position=<-4,  3> velocity=< 2,  0>",
"position=<10, -3> velocity=<-1,  1>",
"position=< 5, 11> velocity=< 1, -2>",
"position=< 4,  7> velocity=< 0, -1>",
"position=< 8, -2> velocity=< 0,  1>",
"position=<15,  0> velocity=<-2,  0>",
"position=< 1,  6> velocity=< 1,  0>",
"position=< 8,  9> velocity=< 0, -1>",
"position=< 3,  3> velocity=<-1,  1>",
"position=< 0,  5> velocity=< 0, -1>",
"position=<-2,  2> velocity=< 2,  0>",
"position=< 5, -2> velocity=< 1,  2>",
"position=< 1,  4> velocity=< 2,  1>",
"position=<-2,  7> velocity=< 2, -2>",
"position=< 3,  6> velocity=<-1, -1>",
"position=< 5,  0> velocity=< 1,  0>",
"position=<-6,  0> velocity=< 2,  0>",
"position=< 5,  9> velocity=< 1, -2>",
"position=<14,  7> velocity=<-2,  0>",
"position=<-3,  6> velocity=< 2, -1>",
]

f = open("input", "r")
inputs = f.read().splitlines()
f.close()

class Star:
    def __init__(self, x,y,vx,vy):
        self.x = x
        self.y = y
        self.vx = vx
        self.vy = vy
    
    def __str__(self):
        return "Star: (x,y) = ({}, {}), (vx, vy) = ({}, {})".format(
                self.x, self.y, self.vx, self.vy)

def parse(line):
    line = line[10:]
    # print(line)
    line = line.split(",")
    x = int(line[0])
    yvx = line[1].split(">")
    y = int(yvx[0])
    vx = int(yvx[1][11:])
    vy = int(line[2][:-1])
    return Star(x,y,vx,vy)

def new_grid(stars):
    grid = Grid(width, height, ".")
    for s in stars:
        grid.set_elmt(s.x, s.y, "#")
    return grid

def move_stars():
    for s in stars:
        s.x +=  s.vx
        s.y +=  s.vy

def all_stars_present(stars):
    return reduce(lambda acc,e: acc and e.x >=0 and e.y >= 0 and e.y < height, stars, True)

def one_star_origin(stars):
    return reduce(lambda acc,s: acc or (s.x ==0 and s.y == 0), stars, False)
    
# inputs = test

height=130
width =400

c = 0
stars = []
for i in inputs:
    stars.append(parse(i))

# while (not one_star_origin(stars)):
while (not all_stars_present(stars)):
    c += 1
    move_stars()

print(c)
# new_grid(stars).print_grid()


