import sys
sys.path.insert(1, "..")
from grid import *

grid_sz = 301

def create_grid(serial_nb):
    grid = Grid(grid_sz,grid_sz)
    for y in range(1,grid_sz):
        for x in range(1,grid_sz):
            grid.set_elmt(x,y, calculate_power_level(x,y, serial_nb))
    return grid

def calculate_power_level(x, y, serial_nb):
    rack_id = x + 10
    pwr = rack_id * y
    pwr += serial_nb
    pwr *= rack_id
    pwr = int(str(pwr // 100)[-1])
    pwr -= 5
    return pwr

def square_pwr(x,y,grid,square_sz):
    s = 0
    for xi in range(square_sz):
        for yi in range(square_sz):
            s += grid.get_elmt(x+xi, y+yi)
    return s

def find_max_square(grid, square_sz):
    m = -10000
    coord = None
    for x in range(1,grid_sz-square_sz+1):
        for y in range(1, grid_sz-square_sz+1):
            pwr = square_pwr(x,y,grid, square_sz)
            if pwr > m:
                m = pwr
                coord = (x,y)
    return (m, coord)

def line_pwr(x,y, sz):
    s = 0
    for xi in range(sz):
        s += grid.get_elmt(x+xi, y)
    return s


serial_nb = 7803
# serial_nb = 18 
mpwr = -50000
mcoord = None
msz = None

# grid_sz = 5
grid = create_grid(serial_nb)


##### Method1
# for square_sz in range(1,301):
    # for x in range(1,grid_sz-square_sz+1):
        # lines = [ line_pwr(x, 1+i, square_sz) for i in range(square_sz) ]
        # for y in range(1, grid_sz-square_sz):
            # pwr = sum(lines)
            # if pwr > mpwr:
                # mpwr = pwr
                # mcoord = (x,y)
                # msz = square_sz
            # lines[(y-1)%square_sz] = line_pwr(x, y+square_sz, square_sz)

##### Method2
def create_sum_grid(grid):
    for x in range(2, grid_sz):
        val = grid.get_elmt(x,1) + grid.get_elmt(x-1,1)
        grid.set_elmt(x,1, val)
    for y in range(2, grid_sz):
        val = grid.get_elmt(1,y) + grid. get_elmt(1,y-1)
        grid.set_elmt(1,y, val)
    for y in range(2,grid_sz):
        for x in range(2,grid_sz):
            val = grid.get_elmt(x,y)
            val += grid.get_elmt(x, y-1)
            val += grid.get_elmt(x-1, y)
            val -= grid.get_elmt(x-1, y-1)
            grid.set_elmt(x,y, val)
    return grid

def square_pwr_sum(x,y,square_sz, grid):
    val =  grid.get_elmt(x+square_sz-1, y+square_sz-1)
    if x > 1:
        val -= grid.get_elmt(x-1, y+square_sz-1) 
    if y > 1:
        val -= grid.get_elmt(x+square_sz-1, y-1)
    if x > 1 and y > 1:
        val += grid.get_elmt(x-1,y-1)
    return val


# grid.print_grid(sep="\t")
grid = create_sum_grid(grid)
# grid.print_grid(sep="\t")
for square_sz in range(1,301):
    for x in range(1,grid_sz-square_sz+1):
        for y in range(1, grid_sz-square_sz+1):
            pwr = square_pwr_sum(x,y,square_sz, grid)
            if pwr > mpwr:
                mpwr = pwr
                mcoord = (x,y)
                msz = square_sz

print(mcoord, msz, mpwr)





