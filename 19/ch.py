from enum import Enum
from copy import copy
import sys
sys.path.insert(1, "..")

fname = "input"
# fname = "test"

f = open(fname,"r")
puzzle = f.read().splitlines()
f.close()
s = [0, 0, 0, 0, 0, 0]

class Instr():
    def __init__(self, name, a, b, c):
        self.name = name
        self.a = a
        self.b = b
        self.c = c
        self.run = instrs[name]

    def ex(self):
        s[self.c] = self.run(self.a,self.b)

    def __repr__(self):
        return "{} {} {} {}".format(self.name, self.a, self.b, self.c)

instrs = {
        "addr": (lambda a,b:  s[a] + s[b]),
        "addi"  : lambda a,b:  s[a] + b, 
        "mulr"  : lambda a,b:  s[a] * s[b], 
        "muli"  : lambda a,b:  s[a] * b, 
        "banr"  : lambda a,b:  s[a] & s[b], 
        "bani"  : lambda a,b:  s[a] & b, 
        "borr"  : lambda a,b:  s[a] | s[b], 
        "bori"  : lambda a,b:  s[a] | b, 
        "setr"  : lambda a,b:  s[a], 
        "seti"  : lambda a,b:  a, 
        "gtir"  : lambda a,b:  1 if a > s[b] else 0 , 
        "gtri"  : lambda a,b:  1 if s[a] > b else 0, 
        "gtrr"  : lambda a,b:  1 if s[a] > s[b] else 0 , 
        "eqir"  : lambda a,b:  1 if a == s[b] else 0 , 
        "eqri"  : lambda a,b:  1 if s[a] == b else 0 , 
        "eqrr"  : lambda a,b:  1 if s[a] == s[b] else 0 , 
        }


#Set instruction pointer
ip = int(puzzle[0][4])

#transform the lines into list of instructions
code = puzzle[1:]
for i, line in enumerate(code):
    name, a, b, c = line.split()
    code[i] = Instr(name, int(a), int(b), int(c))

    
c = 0
while (s[ip] >= 0 and s[ip] < len(code)):
    step = 1
    instr = code[s[ip]]
    if (c%step == 0):
        print("ip={} {} {}".format(ip, s, instr), end =" ")
    instr.ex()
    if (c%step == 0):
        print(s)
    s[ip] = s[ip] + 1
    c = c + 1

print(s)

