from enum import Enum
from copy import copy
import sys
sys.path.insert(1, "..")

fname = "input"

f = open(fname,"r")
puzzle = f.read().splitlines()
f.close()
s = 4 * [0]

class Instr():
    def __init__(self, name, opcode, run):
        self.name = name
        self.opcode = opcode
        self.run = run 

    def exec(self, a,b,c):
        s[c] = self.run(a,b)

    def __repr__(self):
        return self.name 

instrs = []
instrs.append(Instr("addr", None, (lambda a,b:  s[a] + s[b])))
instrs.append(Instr("addi", None, (lambda a,b:  s[a] + b)))
instrs.append(Instr("mulr", None, (lambda a,b:  s[a] * s[b])))
instrs.append(Instr("muli", None, (lambda a,b:  s[a] * b)))
instrs.append(Instr("banr", None, (lambda a,b:  s[a] & s[b])))
instrs.append(Instr("bani", None, (lambda a,b:  s[a] & b)))
instrs.append(Instr("borr", None, (lambda a,b:  s[a] | s[b])))
instrs.append(Instr("bori", None, (lambda a,b:  s[a] | b)))
instrs.append(Instr("setr", None, (lambda a,b:  s[a])))
instrs.append(Instr("seti", None, (lambda a,b:  a)))
instrs.append(Instr("gtir", None, (lambda a,b:  1 if a > s[b] else 0 )))
instrs.append(Instr("gtri", None, (lambda a,b:  1 if s[a] > b else 0 )) )
instrs.append(Instr("gtrr", None, (lambda a,b:  1 if s[a] > s[b] else 0 )))
instrs.append(Instr("eqir", None, (lambda a,b:  1 if a == s[b] else 0 )))
instrs.append(Instr("eqri", None, (lambda a,b:  1 if s[a] == b else 0 )) )
instrs.append(Instr("eqrr", None, (lambda a,b:  1 if s[a] == s[b] else 0 )))


class Sample():
    def __init__(self, inputs):
        self.before = [int(i) for i in inputs[0][9:-1].split(",")]
        self.command = [int(i) for i in inputs[1].split()]
        self.after = [int(i) for i in inputs[2][9:-1].split(",")]

    def __repr__(self):
        return "Before: {} --- {} --- After: {}".format( \
                self.before, self.command, self.after)

samples = []
# puzzle = ["Before: [3, 2, 1, 1]","9 2 1 2","After:  [3, 2, 2, 1]"]
for i in range(0,len(puzzle),4):
    samples.append(Sample(puzzle[i:i+3]))
   
results = []
for sample in samples:
    potential_instrs = []
    for i in instrs:
        s = copy(sample.before)
        i.exec(sample.command[1],sample.command[2],sample.command[3])
        if s == sample.after:
            potential_instrs.append(i)
    results.append([sample, potential_instrs])

iset = {}
for i in range(16):
    op = list(filter(lambda r: r[0].command[0] == i, results))
    instructions = copy(instrs)
    for j in op:
        instructions = [ val for val in instructions if val in j[1]]
    iset[str(i)] = instructions

has_reduced = True
opset = {}
while has_reduced:
    has_reduced = False
    for k,v in iset.items():
        if len(v) == 1:
            val = v[0]
            opset[k] = val
            for v2 in iset.values():
                if val in v2:
                    v2.remove(val)
            has_reduced = True

print(opset)
print(sorted(list(opset.keys())))

f = open("input2","r")
code = f.read().splitlines()
f.close()

s = 4 * [0]
for line in code:
    line = [int(i) for i in line.split()]
    opset[str(line[0])].exec(line[1], line[2], line[3])
print(s)


