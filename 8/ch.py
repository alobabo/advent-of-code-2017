

test = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"
# test = "1 6 0 2 1 1 1 1 1 1 1 1"

f = open("input", "r")
inputs = f.read()
f.close()

# inputs = test
inputs = [ int(i) for i in inputs.split() ]


s = 0

class Node:
    def __init__(self, children, metas):
        self.children = children
        self.metas = metas
        self



def parse(t):
    nb_children = t.pop(0)
    nb_meta = t.pop(0)
    res = 0
    if nb_children == 0:
        for _ in range(nb_meta):
            res += t.pop(0)
        return res
    else:
        tab = []
        for _ in range(nb_children):
            v = parse(t)
            tab.append(v)
        for _ in range(nb_meta):
            meta = t.pop(0)
            if meta <= len(tab):
                res += tab[meta-1] 
        return res
    
print(parse(inputs))


