# 1D array simulating 2D array with extra borders for border case
#   1 2        0 0 0 0
#   3 4    =>  0 1 2 0 => [0 0 0 0 0 1 2 0 0 3 4 0 0 0 0 0]
#              0 3 4 0
#              0 0 0 0 
import sys
class Grid:
    def __init__(self, width, height, val="."):
        self.width = width
        self.height = height
        self.rwidth = width + 2     # real width
        self.rheight = height + 2   # real height 
        self.grid = [ None for _ in range(0,self.rwidth*self.rheight) ]
        for x in range(1, width+1):
            for y in range(1, height+1):
                self.grid[y*(self.rwidth) + x] = val

    def print_grid(self, sep="", show=lambda x: x):
        for y in range(1, self.height+1):
            for x in range(1, self.width+1):
                v = self.grid[y*(self.rwidth) + x]
                print(show(v), end=sep)
            print()

    def set_elmt(self, x, y, val):
        self.grid[(y+1)*(self.rwidth)+(x+1)] = val

    def get_elmt(self, x, y):
        return self.grid[(y+1)*(self.rwidth)+(x+1)]
        

# Tests
if __name__ == "__main__":
    grid = Grid(2,2,val=1)
    grid.print_grid(sep=" ")
    print()
    grid.set_elmt(1,0,2)
    grid.set_elmt(0,1,3)
    grid.set_elmt(1,1,4)
    grid.print_grid(sep=" ")
    assert(grid.get_elmt(0,0) == 1)
    assert(grid.get_elmt(0,1) == 3)
    assert(grid.get_elmt(-1,0) is None)
    assert(grid.get_elmt(-1,-1) is None)
    assert(grid.get_elmt(1,2) is None)
    assert(grid.get_elmt(1,-2) is None)


