import sys
sys.path.insert(1, "..")
from grid import *

fname = "input"
#fname = "test"
#fname = "test4"
#fname = "test6"

width = None
height = None
cave = None
cheat = 0
elves = []
goblins = []

class Mob:
    def __init__(self, is_elf,x,y):
        global cheat
        self.is_elf = is_elf
        if is_elf:
            self.targets = goblins
        else:
            self.targets = elves
        self.x = x
        self.y = y
        self.hp = 200
        if is_elf:
            self.atk = 3 + cheat
        else:
            self.atk = 3 

        
    #Return a list of ennemy mob that self can attack else None
    def move(self):
        can_atk = False
        pos = (self.x, self.y)
        #remove self from the map so it does not account as occupied space
        cave.set_elmt(*pos, ".")
        targets = in_range(self.targets)
        # print("Targets: {}".format(targets))
        cells_next_to_targets = set(sum([t[1] for t in targets], []))
        # print("Cells nexts to targets: {}".format(cells_next_to_targets))

        #If there are no reachable cells we end the turn of the mob
        if len(cells_next_to_targets) == 0: 
            cave.set_elmt(*pos, print_element(self))
            return None

        #Move when we are not already in a target cell 
        if pos not in cells_next_to_targets: 
            #Nearest targets
            nearests = bfs([[pos]], [], 0, cells_next_to_targets)
            if nearests is None: #targets are unreachable
                cave.set_elmt(*pos, print_element(self))
                return None

            # print("Nearests: {}".format(nearests))

            #Pick target destination by read order
            chosen_target = [ p[-1] for p in nearests ]
            chosen_target = min(chosen_target, key=lambda c: cell_to_int(*c))
            # print("Chosen target: {}".format(chosen_target))

            #Pick paths by read order
            chosen_path = list(filter(lambda p: p[-1] == chosen_target, nearests))
            chosen_path = min(chosen_path, key= lambda p: cell_to_int(*p[1]))
            # print("Chosen path: {}".format(chosen_path))

            #Move mob
            next_pos = chosen_path[1]
            self.x = next_pos[0]
            self.y = next_pos[1]
            cave.set_elmt(*next_pos, print_element(self))

            # cave.print_grid(show=print_element)
            # print()
            # sys.exit()

            if len(chosen_path) <= 2: 
                can_atk = True
        else: #when mob is already next to a target we do not move
            cave.set_elmt(*pos, print_element(self))
            can_atk = True
        #Return mobs that we may attack
        if can_atk:
            targets = list(filter(lambda t: (self.x, self.y) in t[1], targets))
            targets = [t[0] for t in targets]
            return targets
        else:
            return None

    def attack(self, targets):
        if targets is None: #When we are not in range for attacking
            return
        min_hp = min([t.hp for t in targets])
        #Keep mob with least hp
        weakests = list(filter(lambda t: t.hp == min_hp, targets))
        #Among the weakests chose by read order
        target = min(weakests, key= lambda t: cell_to_int(t.x, t.y))

        target.hp = target.hp - self.atk
        # print("{} attacks: {} left with {} hp".format(
            # print_element(self), print_element(target), target.hp))
        #if target dies
        if target.hp <= 0:
            cave.set_elmt(target.x, target.y, ".")
            self.targets.remove(target)


    def turn(self):
        #Move and see if we are in range for attacking
        targets = self.move()
        #Attack
        self.attack(targets)


def in_range(targets):
    in_range = []
    for t in targets: 
        in_range.append((t, empty_cells_around(t.x, t.y)))
    return in_range

def leq(x1,y1,x2,y2):
    if y1 < y2:
        return True
    if y1 == y2 and x1 > x2:
        return True
    if y1 == y2 and x1 == x2:
        return True
    return False

def cell_to_int(x,y):
    return y*100000 + x

def bfs(current_paths, visited, distance, targets):
    #The targets may be unreachable
    if len(current_paths) == 0:
        return None

    # print(current_paths)
    next_paths = []
    res = [] 
    for p in current_paths:
        c = p[-1]  #current cell is the last one in paths
        #if cell is a target
        if c in targets:
            res.append(p)
            continue
        #cell has already been visited
        if c in visited:  
            continue
        for ncell in empty_cells_around(*c):
            # print("empty cell around {} are {}".format(c, empty_cells_around(*c)))
            next_paths.append(p + [ncell])
        visited.append(c)

    if len(res) == 0:
        return bfs(next_paths, visited, distance + 1, targets)
    else:
        return res

def empty_cells_around(x,y):
    empty_cells = []
    positions = ((x, y-1), (x-1, y), (x+1, y), (x, y+1))
    for p in positions:
        if p[0] >= 0 and p[0] < width and \
        p[1] >= 0 and p[1] < height and \
        cave.get_elmt(p[0], p[1]) == ".":
            empty_cells.append(p)
    return empty_cells


def print_element(e):
    if isinstance(e, Mob):
        if e.is_elf:
            return "E"
        else:
            return "G"
    else:
        return e

def init_map():
    global width
    global height
    global cave
    global elves 
    global goblins
    f = open(fname,"r")
    puzzle = f.read().splitlines()
    f.close()
    width = len(puzzle[0]) - 2
    height = len(puzzle) - 2
    # print("width: {}, height: {}".format(width, height))
    cave = Grid(width, height)
    for x in range(1, width+1):
        for y in range(1, height+1):
            if puzzle[y][x] == ".":
                cave.set_elmt(x-1,y-1, ".")
            elif puzzle[y][x] == "#":
                cave.set_elmt(x-1,y-1, "#")
            elif puzzle[y][x] == "E":
                elf = Mob(True, x-1, y-1)
                cave.set_elmt(x-1,y-1, elf )
                elves.append(elf)
            elif puzzle[y][x] == "G":
                goblin = Mob(False, x-1, y-1)
                cave.set_elmt(x-1,y-1, goblin)
                goblins.append(goblin)

def turn():
    mobs = sorted((elves + goblins), key=lambda m: cell_to_int(m.x, m.y))
    for mob in mobs:
        if isinstance(mob, Mob) and mob.hp > 0:
            # print("Mob: ({}, {})".format(mob.x, mob.y))
            mob.turn()

init_map()
cave.print_grid(show=print_element)
print()
c = 0
nb_elves = len(elves)
# for _ in range(3):
while (len(elves) > 0 and len(goblins) > 0):
    turn()
    c = c + 1
    if (len(elves) < nb_elves):
        c = 0
        cheat = cheat+1
        goblins = []
        elves = []
        init_map()
   
c = c-1
cave.print_grid(show=print_element)
print("Battle ends at round {}".format(c))
total_hp =sum([m.hp for m in elves+goblins])
print("Total HP left is {}".format(total_hp))
print("Result of the battle is {}".format(total_hp * c))
# Movement:
# - Pos to aim for
# - Reachable pos
# - Nearest pos
# - Chosen pos

#Attack
# - chose weakest ennemy in range

