from collections import namedtuple 
from enum import Enum
import sys
sys.path.insert(0, '../grid.py')

test = [
"[1518-11-01 00:00] Guard #10 begins shift",
"[1518-11-01 00:05] falls asleep",
"[1518-11-01 00:25] wakes up",
"[1518-11-01 00:30] falls asleep",
"[1518-11-01 00:55] wakes up",
"[1518-11-01 23:58] Guard #99 begins shift",
"[1518-11-02 00:40] falls asleep",
"[1518-11-02 00:50] wakes up",
"[1518-11-03 00:05] Guard #10 begins shift",
"[1518-11-03 00:24] falls asleep",
"[1518-11-03 00:29] wakes up",
"[1518-11-04 00:02] Guard #99 begins shift",
"[1518-11-04 00:36] falls asleep",
"[1518-11-04 00:46] wakes up",
"[1518-11-05 00:03] Guard #99 begins shift",
"[1518-11-05 00:45] falls asleep",
"[1518-11-05 00:55] wakes up"
 ]

test2 = [
"[1518-11-03 00:29] wakes up",
"[1518-11-01 00:00] Guard #10 begins shift",
"[1518-11-01 00:25] wakes up",
"[1518-11-01 00:30] falls asleep",
"[1518-11-01 00:55] wakes up",
"[1518-11-02 00:40] falls asleep",
"[1518-11-01 00:05] falls asleep",
"[1518-11-01 23:58] Guard #99 begins shift",
"[1518-11-02 00:50] wakes up",
"[1518-11-03 00:05] Guard #10 begins shift",
"[1518-11-03 00:24] falls asleep",
"[1518-11-05 00:03] Guard #99 begins shift",
"[1518-11-04 00:02] Guard #99 begins shift",
"[1518-11-04 00:46] wakes up",
"[1518-11-05 00:45] falls asleep",
"[1518-11-04 00:36] falls asleep",
"[1518-11-05 00:55] wakes up"
 ]
f = open("input", "r")
inputs = f.read().splitlines()
f.close()

Record = namedtuple("Record", ["date", "id", "sleep"])
def parse(lines):
    global records
    date = None
    ID = None
    sleep = None
    for line in lines:
        line = line.split()
        if line[2] == "Guard":
            if date:
                records.append(Record(date, ID, sleep))
            date = line[0][6:]
            ID = int(line[3][1:])
            sleep = ["." for _ in range(60)]
        elif line[2] == "wakes":
            start = int(line[1][3:5])
            for i in range(start, 60):
                sleep[i] = "."
        elif line[2] == "falls":
            start = int(line[1][3:5])
            for i in range(start, 60):
                sleep[i] = "#"
        else:
            print("Parse error: ", line[2])
            exit(-1);
    records.append(Record(date, ID, sleep))
    return 

def count_sleep(ide, sleep):
    global guards
    for (i,s) in enumerate(sleep):
        if s == "#":
            guards[ide]["total"] += 1
            guards[ide]["sleeps"][i] += 1


def print_rec():
    for rec in records:
        print(rec.date, "\t", rec.id, "\t")
        for s in rec.sleep:
            print(s, end="")
        print()
        print()

# inputs = test
records= []
parse(sorted(inputs))
# print_rec()

guards = {}
for rec in records:
    if rec.id in guards.keys(): 
        count_sleep(rec.id, rec.sleep)
    else: 
        dic = {}
        for i in range(60):
            dic[i] = 0
        guards[rec.id] = {"total": 0, "sleeps": dic}
        count_sleep(rec.id, rec.sleep)

guard = max(guards.keys(), key=(lambda k: guards[k]["total"]))
dic = {}
for k in guards.keys():
    minute = max(guards[k]["sleeps"].keys(), key=(lambda l: guards[k]["sleeps"][l]))
    dic[k] = (minute, guards[k]["sleeps"][minute])

print(dic)
guard = max(dic.keys(), key=(lambda k: dic[k][1]))
print(guard* dic[guard][0])


