import sys
import copy
sys.path.insert(1, "..")
from grid import *

fname = "input"
# fname = "test"

width = None
height = None
waters = [[500, 0]] #Source of waters
field = None
min_x = None
max_x = None
max_y = None
min_y = None


def parse(line):
    res = []
    line = line.split(", ")
    static = int(line[0][2:])
    loop = [int(i) for i in line[1][2:].split("..")]
    if line[0][0] == "x":
        for i in range(loop[0], loop[1]+1):
            res.append((static, i))
    else: 
        for i in range(loop[0], loop[1]+1):
            res.append((i, static))
    return res

def init_map():
    global width
    global height
    global field
    global min_x
    global max_x
    global min_y
    global max_y
    f = open(fname,"r")
    puzzle = f.read().splitlines()
    f.close()
    clays = []
    for line in puzzle:
        res = parse(line)
        clays = clays + res 
    clays_x = [ clay[0] for clay in clays ]
    min_x = min(clays_x)-1
    max_x = max(clays_x)+2
    width = max_x - min_x
    clays_y = [clay[1] for clay in clays ]
    min_y = min(clays_y)
    max_y = max(clays_y) + 1
    field = Grid(width, max_y, val=".", min_x=min_x)
    for clay in clays:
        field.set_elmt(clay[0], clay[1], "#")
    field.set_elmt(500,0, "+")

def move_water():
    for w in copy.copy(waters):
        w[1] = w[1] + 1
        #As long as the cell below current is not out of the field or clay
        while(w[1] < max_y and field.get_elmt(w[0],w[1]) != "#"):
            field.set_elmt(*w,"|")
            w[1] = w[1] + 1

        if w[1] >= max_y: 
            #If this source won't bring anything new
            waters.remove(w)
            continue

        #We have reached some clay
        should_go_up = True 

        while(should_go_up):
            w[1] = w[1] - 1
            field.set_elmt(*w, "~")

            #Fill the right side 
            rw = [w[0]+1, w[1]]

            #As long as right side do not find clay and that it's not sand below
            # we extend the right side
            while (field.get_elmt(*rw) != "#" and 
                    (field.get_elmt(rw[0], rw[1]+1) == "#" or \
                    field.get_elmt(rw[0], rw[1]+1) == "~")):
                field.set_elmt(*rw, "~")
                rw[0] = rw[0]+1
            #If we right side encounters clay
            if field.get_elmt(*rw) == "#":
                should_go_up_r = True
            else:
                # Otherwise we add it as a new source
                field.set_elmt(*rw, "|")
                if rw not in waters:
                    waters.append(rw)
                should_go_up_r = False

            #Fill the left side
            lw = [w[0]-1, w[1]]
            while (field.get_elmt(*lw) != "#" and \
                    (field.get_elmt(lw[0], lw[1]+1) == "#" or \
                    field.get_elmt(lw[0], lw[1]+1) == "~" )):
                field.set_elmt(*lw, "~")
                lw[0] = lw[0]-1
            #If we right side encounters clay
            if field.get_elmt(*lw) == "#":
                should_go_up_l = True
            else:
                # Otherwise we add it as a new source
                field.set_elmt(*lw, "|")
                if lw not in waters:
                    waters.append(lw)
                should_go_up_l = False

            should_go_up = should_go_up_r and should_go_up_l

        field.set_elmt(*w, "|")
        rw = [w[0]+1, w[1]]
        lw = [w[0]-1, w[1]]
        while (field.get_elmt(*rw) == '~'):
            field.set_elmt(*rw, "|")
            rw[0] = rw[0]+1
        while (field.get_elmt(*lw) == '~'):
            field.set_elmt(*lw, "|")
            lw[0] = lw[0]-1
        waters.remove(w)
    
init_map()
# field.print_grid()
while len(waters) != 0:
# for _ in range(500):
    move_water()
    # field.print_grid()

c = 0
for x in range(min_x, max_x):
    for y in range(min_y, max_y):
        e = field.get_elmt(x,y)
        if e == "~": #or e == "|" or e=="+":
            c = c + 1

field.print_grid()
print(c)


    # # print("width: {}, height: {}".format(width, height))
    # cave = Grid(width, height)
    # for x in range(1, width+1):
        # for y in range(1, height+1):
            # if puzzle[y][x] == ".":
                # cave.set_elmt(x-1,y-1, ".")
            # elif puzzle[y][x] == "#":
                # cave.set_elmt(x-1,y-1, "#")
            # elif puzzle[y][x] == "E":
                # elf = Mob(True, x-1, y-1)
                # cave.set_elmt(x-1,y-1, elf )
                # elves.append(elf)
            # elif puzzle[y][x] == "G":
                # goblin = Mob(False, x-1, y-1)
                # cave.set_elmt(x-1,y-1, goblin)
                # goblins.append(goblin)

