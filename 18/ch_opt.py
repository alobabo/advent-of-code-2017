import sys
sys.path.insert(1, "..")
from grid import *
import copy

def next_field(field, changed_cells):
    nfield = copy.deepcopy(field)
    done = []
    nchanged_cells = []
    for cell in changed_cells:
        x = cell[0]
        y = cell[1]
        neighbours = get_pos_around(x,y) + [cell]
        for n in neighbours:
            n_has_evolved = False
            if n not in done:
                e = field.get_elmt(*n)
                if should_evolve(*n,e,field):
                    nfield.set_elmt(*n,(e+1)%3)
                    nchanged_cells.append(n)
                    n_has_evolved = True
                done.append(n)
    return (nfield, nchanged_cells)

def get_pos_around(x,y):
    return [(x-1,y-1), (x-1,y), (x-1,y+1),(x,y-1),(x,y+1),(x+1,y-1),(x+1,y),(x+1,y+1)]

def should_evolve(x,y,e,field):
    around = get_elmt_around(x,y,field)
    if e == 0 and around[1] >= 3:
        return True
    elif e == 1 and around[2] >= 3:
        return True
    elif e == 2 and (around[2] < 1 or around[1] < 1):
        return True
    return False

def get_elmt_around(x,y,field):
    around = [0,0,0]
    pos_around = get_pos_around(x,y)
    for pos in pos_around:
        e = field.get_elmt(*pos)
        if e is not None:
            around[e] = around[e]+1
    return around

def count_value(field):
    trees = 0
    lumber = 0
    for x in range(width):
        for y in range(height):
            e = field.get_elmt(x,y)
            if e == 1:
                trees = trees + 1
            elif e == 2:
                lumber = lumber + 1
    return trees * lumber

def print_element(e):
    return symbols[e]

fname = "input"
# fname = "test"
# fname = "test2"
f = open(fname, "r")
puzzle = f.read().splitlines()
f.close()

width = len(puzzle[0])
height = len(puzzle)
pos = (width-5, 2)
world = Grid(width, height)
changed = []
symbols = [".", "|", "#", "?"]
for y,line in enumerate(puzzle):
    for x,element in enumerate(line):
        v = None
        if element == ".":
            v = 0
        elif element == "|":
            v = 1
        elif element == "#":
            v = 2
        world.set_elmt(x,y, v)
        changed.append((x,y))


# world.print_grid(show=print_element)
for i in range(100):
# for i in range(20):
# for i in range(1000000000):
    # print(should_evolve(*pos, 2,world))
    # print(get_elmt_around(*pos,world))
    world,changed = next_field(world,changed)
    # e = world.get_elmt(*pos)
    # world.set_elmt(*pos, 3)
    # world.print_grid(show=print_element)
    # world.set_elmt(*pos, e)
   


# world.print_grid(show=print_element)
print(count_value(world))


        

