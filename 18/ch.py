import sys
sys.path.insert(1, "..")
# from grid import *
from opt_grid import *

fname = "input"
# fname = "test"
# fname = "test2"
f = open(fname, "r")
puzzle = f.read().splitlines()
f.close()

width = len(puzzle[0])
height = len(puzzle)
field = Grid(width, height)
for y,line in enumerate(puzzle):
    for x,element in enumerate(line):
        field.set_elmt(x,y, element)


def next_field():
    nfield = Grid(width, height)
    for x in range(width):
        for y in range(height):
            nelmt = next_elmt(x,y)
            nfield.set_elmt(x,y,nelmt)
    return nfield

def next_elmt(x,y):
    e = field.get_elmt(x,y)
    around = get_elmt_around(x,y,field)
    # print("({},{}) -> around = {}".format(x,y,around))
    if e == ".":
        if around[1] >= 3:
            return "|"
    elif e == "|":
        if around[2] >= 3:
            return "#"
    elif e == "#":
        if around[2] < 1 or around[1] < 1:
            return "."
    else:
        print("Error ({},{}) out of bounds".format(x,y))
        sys.exit()
    return e

def get_elmt_around(x,y,field):
    around = [0,0,0]
    pos_around = [(x-1,y-1), (x-1,y), (x-1,y+1),(x,y-1),(x,y+1),(x+1,y-1),(x+1,y),(x+1,y+1)]
    for pos in pos_around:
        e = field.get_elmt(*pos)
        if e == ".":
            around[0] = around[0]+1
        elif e == "|":
            around[1] = around[1]+1
        elif e == "#":
            around[2] = around[2]+1
    return around

def count_value():
    trees = 0
    lumber = 0
    for x in range(width):
        for y in range(height):
            e = field.get_elmt(x,y)
            if e == "|":
                trees = trees + 1
            elif e == "#":
                lumber = lumber + 1
    return trees * lumber


# field.print_grid()
print(width, height)
for i in range(1000000000):
    nfield = next_field()
    # nfield.print_grid()
    field = nfield
    print(count_value())
# field.print_grid()
print(count_value())


        

