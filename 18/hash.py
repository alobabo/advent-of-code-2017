import sys
sys.path.insert(1, "..")
# from grid import *
from opt_grid import *


fname = "input"
# fname = "test"
# fname = "test2"
f = open(fname, "r")
puzzle = f.read().splitlines()
f.close()

width = len(puzzle[0])
height = len(puzzle)
ref = 5

hash_grid = Grid(width/5,height/5, val=None)

class Node: 
    def __init__(self,grid):
        self.grid = grid

    def hash(self):
        v = 0
        for x in range(ref):
            for y in range(ref):
                e = self.grid.get_elmt(x,y)
                if e == ".":
                    mult = 1
                elif e == "|":
                    mult = 100
                elif e == "#":
                    mult = 10000
                v = v + mult*(x+y*10)
        return v 

def init_hash_grid():
    for y,line in enumerate(puzzle):
        for x,element in enumerate(line):
            field.set_elmt(x,y, element)




